package com.example.bfu_helper_10

class DayOfSchedule {
    var dayOfWeek: String
    var month: String
    private var date: Int
    var subjects: List<Subject>?

    constructor() {
        dayOfWeek = ""
        month = ""
        date = 0
        subjects = null
    }

    constructor(dayOfWeek: String, month: String, date: Int, subjects: List<Subject>?) {
        this.dayOfWeek = dayOfWeek
        this.month = month
        this.date = date
        this.subjects = subjects
    }

    fun getDate(): String {
        return Integer.toString(date)
    }

    fun setDate(date: Int) {
        this.date = date
    }
}