package com.example.bfu_helper_10

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.bfu_helper_10.CardAdapter.ViewHolder2

class CardAdapter(  //private final List<Subject> subjects;
        private val dayOfSchedule: DayOfSchedule) : RecyclerView.Adapter<ViewHolder2>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder2 {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_card, parent, false)
        return ViewHolder2(view)
    }

    override fun onBindViewHolder(holder: ViewHolder2, position: Int) {
        val subject = dayOfSchedule.subjects!![position]
        holder.subjectImage.setImageResource(subject.image)
        holder.subjectName.text = subject.subject
        holder.time.text = subject.time
        holder.cardView.setOnClickListener {
            MainActivity.fragment!!.TestFoo(subject.subject, subject.lecturer,
                    subject.time, subject.image, dayOfSchedule.dayOfWeek,
                    dayOfSchedule.getDate(), dayOfSchedule.month)

            //
            //MainActivity.mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            //Toast.makeText(v.getContext(), subject.getSubject(), Toast.LENGTH_SHORT).show();
        }
    }

    override fun getItemCount(): Int {
        return dayOfSchedule.subjects!!.size
    }

    class ViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val time: TextView
        val subjectName: TextView
        val subjectImage: ImageView
        val cardView: CardView

        init {
            time = itemView.findViewById<View>(R.id.time) as TextView
            subjectName = itemView.findViewById<View>(R.id.subjectName) as TextView
            subjectImage = itemView.findViewById<View>(R.id.subjectImage) as ImageView
            cardView = itemView.findViewById<View>(R.id.buttonSubject) as CardView
        }
    }
}