package com.example.bfu_helper_10

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.example.bfu_helper_10.HttpController.Functions.getDaysOfTheWeek
import com.example.bfu_helper_10.WindowFragment.Companion.newInstance
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import java.util.*

class MainActivity : AppCompatActivity() {

    var days: List<DayOfSchedule> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val bottomNavigationView = findViewById<View>(R.id.bottom_navigation) as BottomNavigationView
        val main_layout = findViewById<View>(R.id.main_layout) as RelativeLayout
        val recyclerView = findViewById<View>(R.id.list) as RecyclerView

        fragment = newInstance()
        Companion.fragmentManager = supportFragmentManager

        getDaysOfTheWeek("https://grph.ru/bfu_app/get_schedule", """{
    "userId": "604264122",
    "sessionCode": "ed410412dd216bed2daab53eded2a04f"
}""", this, recyclerView, (days as ArrayList<DayOfSchedule>))

        val adapter = DataAdapter(this, days)
        bottomNavigationView.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.page_1 -> {
                    main_layout.setBackgroundColor(Color.parseColor("#E0BDFB"))
                    recyclerView.adapter = null
                }
                R.id.page_2 -> {
                    main_layout.setBackgroundColor(Color.parseColor("#BDC0FB"))
                    recyclerView.adapter = null
                }
                R.id.page_3 -> {
                    main_layout.setBackgroundColor(Color.parseColor("#FFFFFF"))
                    recyclerView.adapter = adapter
                }
                else -> {
                }
            }
            true
        }
    }

    companion object {
        val instance: MainActivity? = null

        // static backdrop
        var mBottomSheetBehavior: BottomSheetBehavior<*>? = null
        @JvmField
        var fragment: WindowFragment? = null
        var fragmentManager: FragmentManager? = null
    }
}