package com.example.bfu_helper_10

class Subject(val time: String, val subject: String, val lecturer: String, val room: String, val image: Int) {

    companion object {
        fun getPictureBasedOnName(name: String): Int {
            //Log.d("string testing", name);
            return if (name == "Функциональный анализ (пр.)" || name == "Функциональный анализ (лек.)") R.raw.fuan
            else if (name == "Базы данных (лек.)") R.raw.database
            else if (name == "Уравнения математической физики (лек.)" || name == "Уравнения математической физики (пр.)") R.raw.phys
            else if (name == "Параллельное программирование в задачах визуализации данных (лек.)") R.raw.gpu
            else if (name == "Элективные курсы по физической культуре") R.raw.fizra
            else if (name == "Android") R.raw.android
            else R.raw.index
        }
    }
}