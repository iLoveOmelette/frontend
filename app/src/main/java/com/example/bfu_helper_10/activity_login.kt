package com.example.bfu_helper_10

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.example.bfu_helper_10.MainActivity

/**
 * Implementation of App Widget functionality.
 */
class activity_login : AppCompatActivity() {
    private var textUsernameLayout: EditText? = null
    private var textPasswordInput: EditText? = null
    private var loginButton: Button? = null
    private var progressBar: ProgressBar? = null
    private var LogoImage: ImageView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // use specific view (activity_login.xml) for this activity
        setContentView(R.layout.activity_login)


        // find view info in res/layout/*
        textUsernameLayout = findViewById(R.id.textUsernameLayout)
        textPasswordInput = findViewById(R.id.textPasswordInput)
        loginButton = findViewById(R.id.loginButton)
        progressBar = findViewById(R.id.progressBar)
        LogoImage = findViewById(R.id.imageView)
        loginButton?.let { it.setOnClickListener(View.OnClickListener { onLoginClicked() }) }
        textUsernameLayout?.let { it.addTextChangedListener(createTextWatcher(textUsernameLayout)) }
        textPasswordInput?.let { it.addTextChangedListener(createTextWatcher(textPasswordInput)) }
    }

    private fun onLoginClicked() {
        val username = textUsernameLayout!!.text.toString()
        val password = textPasswordInput!!.text.toString()
        if (username.isEmpty()) {
            textUsernameLayout!!.error = "Username must not be empty"
        } else if (password.isEmpty()) {
            textPasswordInput!!.error = "Password must not be empty"
        } else {
            performLogin()
        }
    }

    //listener for changes in input layout
    private fun createTextWatcher(textPasswordInput: EditText?): TextWatcher {
        return object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence,
                                           start: Int, count: Int, after: Int) {
                // not needed
            }

            override fun onTextChanged(s: CharSequence,
                                       start: Int, before: Int, count: Int) {
                textPasswordInput!!.error = null
            }

            override fun afterTextChanged(s: Editable) {
                // not needed
            }
        }
    }

    private fun performLogin() {
        textUsernameLayout!!.isEnabled = false
        textPasswordInput!!.isEnabled = false
        loginButton!!.visibility = View.INVISIBLE
        progressBar!!.visibility = View.VISIBLE
        val handler = Handler()
        //setTimeOut({}, 2000)
        handler.postDelayed({ startMainActivity() }, 300)
    }

    private fun startMainActivity() {
        // Search for MainActivity class
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}