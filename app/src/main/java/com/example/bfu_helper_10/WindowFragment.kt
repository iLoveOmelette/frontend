package com.example.bfu_helper_10

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import android.widget.RatingBar
import android.widget.TextView
import androidx.core.view.GestureDetectorCompat
import android.view.LayoutInflater
import android.view.ViewGroup
import android.os.Bundle
import android.util.Log
import com.example.bfu_helper_10.R
import android.widget.ImageButton
import android.view.View.OnTouchListener
import android.view.MotionEvent
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.View
import android.widget.Button
import android.widget.RatingBar.OnRatingBarChangeListener
import androidx.fragment.app.FragmentManager
import com.example.bfu_helper_10.MainActivity
import com.example.bfu_helper_10.WindowFragment

class WindowFragment : BottomSheetDialogFragment() {
    private var rBar: RatingBar? = null
    private val tView: TextView? = null
    private val btn: Button? = null
    private var header: TextView? = null
    private var lecturer: TextView? = null
    private var _view: View? = null
    private var _header = ""
    private var _lecturer = ""
    private var __time = ""
    private var sch_day_of_week = ""
    private var sch_day = ""
    private var sch_month = ""
    private var __image = 0
    private var mGestureDetector: GestureDetectorCompat? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        _view = inflater.inflate(R.layout.fragment_window, container, false)

        //Log.d("test", "onCreate triggered");
        header = _view?.let { it.findViewById<View>(R.id.bdHeader) as TextView }
        lecturer = _view?.let { it.findViewById<View>(R.id.bdTeacher) as TextView }
        val image = _view?.let { it.findViewById<ImageButton>(R.id.bdButton) }
        header!!.text = _header
        lecturer!!.text = _lecturer
        image!!.setImageResource(__image)
        _view?.let {
            it.setOnTouchListener(OnTouchListener { v, event ->
                mGestureDetector!!.onTouchEvent(event)
                true
            })
        }
        mGestureDetector = GestureDetectorCompat(context, object : SimpleOnGestureListener() {
            override fun onFling(e1: MotionEvent, e2: MotionEvent, velocityX: Float, velocityY: Float): Boolean {
                Log.d("touch_event", "onFling")
                return false
            }

            override fun onDown(e: MotionEvent): Boolean {
                Log.d("touch_event", "onDown")
                return false
            }
        })
        return _view
    }

    override fun show(manager: FragmentManager, tag: String?) {
        super.show(manager, tag)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val rating = getView()!!.findViewById<View>(R.id.bdText2) as TextView
        val private_rating = getView()!!.findViewById<View>(R.id.bdText5) as TextView
        rBar = getView()!!.findViewById<View>(R.id.ratingBar1) as RatingBar
        val date_to_set = getView()!!.findViewById<View>(R.id.bdDate) as TextView

        // capitalize first letter, and remaining part is in lower case
        date_to_set.text = sch_day_of_week.substring(0, 1) + sch_day_of_week.substring(1).toLowerCase() + ", " + sch_day + " " + sch_month
        rBar!!.onRatingBarChangeListener = OnRatingBarChangeListener { ratingBar, _rating, fromUser ->

            // Called when the user swipes the RatingBar
            val score = ratingBar.rating
            rating.text = "Рейтинг занятия: $score"
            private_rating.text = "Ваша оценка: $score"
        }
    }

    fun TestFoo(__header: String, __lecturer: String, time: String, image: Int, _dayOfWeek: String, _day: String, _month: String) {
        _header = __header
        _lecturer = __lecturer
        __time = time
        __image = image
        sch_month = _month
        sch_day = _day
        sch_day_of_week = _dayOfWeek
        this.show(fragmentManager as FragmentManager, "tag")
    }

    companion object {
        const val TAG = "ActionBottomDialog"
        @JvmStatic
        fun newInstance(): WindowFragment {
            //Log.d("test", "newInstance triggered");
            return WindowFragment()
        }
    }
}