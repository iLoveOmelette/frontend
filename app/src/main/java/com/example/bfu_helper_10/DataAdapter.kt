package com.example.bfu_helper_10

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.RecycledViewPool

class DataAdapter internal constructor(context: Context?, daysOfSchedule: List<DayOfSchedule>?) : RecyclerView.Adapter<DataAdapter.ViewHolder>() {
    private val inflater: LayoutInflater
    private val daysOfSchedule: List<DayOfSchedule>?
    private val viewPool = RecycledViewPool()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.list_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val dos = daysOfSchedule!![position]
        holder.date.text = dos.dayOfWeek + ", " + dos.getDate() + " " + dos.month
        val layoutManager = LinearLayoutManager(holder.rv.context, LinearLayoutManager.VERTICAL, false)
        layoutManager.initialPrefetchItemCount = dos.subjects!!.size
        val cardAdapter = CardAdapter(dos)
        holder.rv.layoutManager = layoutManager
        holder.rv.adapter = cardAdapter
        holder.rv.setRecycledViewPool(viewPool)
    }

    override fun getItemCount(): Int {
        return daysOfSchedule?.size ?: 0
    }

    class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        val date: TextView
        val rv: RecyclerView

        init {
            date = view.findViewById<View>(R.id.date) as TextView
            rv = view.findViewById<View>(R.id.cardId) as RecyclerView
        }
    }

    init {
        inflater = LayoutInflater.from(context)
        this.daysOfSchedule = daysOfSchedule
    }
}